HS_FILES = Main.hs Graph.hs Configuration.hs ISTree.hs
DOC_DIR = doc

.PHONY: clean doc docdir exec main test

main: $(HS_FILES)
	ghc -o main Main.hs

exec: main
	./main

doc: $(HS_FILES) docdir
	haddock -o $(DOC_DIR) --html $(HS_FILES)

docdir:
	mkdir -p $(DOC_DIR)

test: clean
	doctest $(HS_FILES)

clean:
	rm -f *.o *.hi
	rm -f main
	rm -rf doc

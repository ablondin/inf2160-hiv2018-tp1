# Travail pratique 1 - Énumération de sous-arbres induits en Haskell

## Description

Décrire ici le projet de façon assez générale. Vous pouvez reprendre tels quels
des passages disponibles dans l'énoncé si vous voulez, mais n'hésitez pas à
l'adapter à votre propre style.

## Auteur

Indiquer ici l'auteur et son code permanent. Par exemple

Bob LeMineur (LEMB12345678)

## Fonctionnement

Expliquer comment faire fonctionner le projet, en particulier

- avertir l'utilisateur qu'il doit installer certaines dépendances (dire
  lesquelles ou référer à la section);
- expliquer comment compiler le projet;
- expliquer comment exécuter le projet;
- expliquer comment lancer les tests automatiques;
- expliquer comment générer la documentation.

Utilisez bien le format Markdown pour mettre des extraits de commande, avec le
résultat attendu s'il y a lieu.

Donnez au moins un exemple du résultat final de votre projet. Par exemple,
ajoutez l'image de l'arbre obtenu en prenant un graphe en particulier, en
expliquant la commande qu'il vous a fallu entrer.

## Contenu du projet

Décrire *tous* les fichiers contenus dans le dépôt. Utilisez une liste à puces.

## Dépendances

Indiquer toutes les dépendances pour faire fonctionner ce projet. Dans chaque
cas, donnez un lien vers le site officiel de cette dépendance.

## Références

Indiquer ici vos références s'il y a lieu. Si vous n'en avez pas utilisé,
supprimez cette section.

## Statut

Indiquer si le projet est complété ou s'il y a eu des problèmes/limitations.

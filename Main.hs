{-|
Module      : Main
Description : A module for producing a Graphviz output tree
Copyright   : (c) Alexandre Blondin Massé
License     : GPL-3
Maintainer  : blondin_masse.alexandre@uqam.ca
Stability   : experimental

This module is useful for producing a Graphviz output of a tree representing
all induced subtrees of a given graph. The induced subtrees appear uniquely in
the leaves of the produced tree.
 -}

module Main where

import Graph hiding (toGraphvizString)
import Configuration hiding (toGraphvizString)
import ISTree hiding (toGraphvizString)
import qualified Configuration (toGraphvizString)
import qualified ISTree (toGraphvizString)

import System.Process
import Data.Traversable

-- Change those values for changing directories and filenames
name = "node_"
directory = "tmp/"
dotFileName name = directory ++ name ++ ".dot"
pngFileName name = directory ++ name ++ ".png"
treeDotFileName = directory ++ "tree.dot"
treePngFileName = "tree.png"

-- Change those values for generating other trees
graph = cycleGraph 4
tree = buildISTree graph
allConfigurations = namedConfigurations name tree

-- | Main function for generating induced subtrees
main = do
    callCommand $ "mkdir -p " ++ directory
    forM allConfigurations $ \(name,configuration) -> do
        putStrLn $ "Writing to file " ++ dotFileName name
        writeFile (dotFileName name) (Configuration.toGraphvizString configuration)
        putStrLn $ "Calling neato on " ++ (dotFileName name)
        callCommand $ "neato -Tpng -o " ++ (pngFileName name) ++ " " ++ (dotFileName name)
    putStrLn "Writing to file graph.dot"
    writeFile treeDotFileName $ ISTree.toGraphvizString directory name tree
    putStrLn $ "Calling dot on " ++ treeDotFileName
    callCommand $ "dot -Tpng -o " ++ treePngFileName ++ " " ++ treeDotFileName
    callCommand $ "rm -rf " ++ directory
